import React, { Component } from "react";
import { Layout } from "antd";
import SidebarLayout from "../components/Sidebar";
// import BreadcrumbLayout from "../components/Breadcrumb";
import HeaderLayout from "../components/Header";
import FooterLayout from "../components/Footer";
import "./style.scss";
import history from "../history";

const { Content } = Layout;

class LayoutComponent extends Component {
  componentDidMount() {
    if (localStorage.getItem("accessToken") === null) {
      history.push("login");
    }
  }
  render() {
    return (
      <Layout>
        <SidebarLayout />
        <Layout>
          <HeaderLayout />
          <Content className="content-container">
            {/* <BreadcrumbLayout data={this.props.breadCrumb} /> */}
            {this.props.children}
          </Content>
          <FooterLayout />
        </Layout>
      </Layout>
    );
  }
}

export default LayoutComponent;
