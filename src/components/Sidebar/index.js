import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { Layout, Row, Icon, Avatar, Menu, Col, Badge, Collapse, Divider } from "antd";
import "./style.scss";

const { Sider } = Layout;
const { Panel } = Collapse;

class SidebarLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: [],
      collapsed: false
    };
  }

  componentDidMount() {
    this.setRoute();
  }

  // toggleCollapse = () => {
  //   this.setState({ collapsed: !this.state.collapsed });
  // };

  onClickMenu = async e => {
    this.setState({ id: e.keyPath });
    localStorage.setItem("idMenu", JSON.stringify(e.keyPath));
  };

  setRoute = () => {
    const idMenu = JSON.parse(localStorage.getItem("idMenu"));
    this.setState({ id: idMenu ? idMenu : ["1"] });
  };

  render() {
    if (this.state.id.length === 0) {
      return <div />;
    }
    return (
      // <Sider collapsed={this.state.collapsed} className="sidebar-container">
      <Sider className="sidebar-container">
        {/* <Row type="flex" justify={this.state.collapsed ? "center" : "space-between"} align="middle" className="sidebar-row-logo"> */}
        {/* {!this.state.collapsed && (
            <h1 className="sidebar-sam">
              SAM <span className="sidebar-today">Today</span>
            </h1>
          )} */}
        {/* <Icon type="menu" style={{ fontSize: 18 }} className="cursor-pointer" onClick={this.toggleCollapse} />
        </Row> */}
        {/* <Row className="sidebar-row-menu" type="flex" align="middle">
          <Col xs={this.state.collapsed ? 24 : 8} className={this.state.collapsed ? "text-center" : "text-left"}>
            <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" size={40} />
          </Col>
          <Col xs={this.state.collapsed ? 0 : 16} className="text-right">
            <Badge dot className="sidebar-avatar-icon">
              <Icon type="message" />
            </Badge>
            <Badge dot className="sidebar-avatar-icon">
              <Icon type="bell" />
            </Badge>
            <Badge className="sidebar-avatar-icon">
              <Icon type="logout" />
            </Badge>
          </Col>
        </Row> */}
        {/* <div style={this.state.collapsed ? { display: "none" } : { display: "block" }} className="sidebar-group">
          <Collapse bordered={false} defaultActiveKey={["1"]} className="sidebar-name">
            <Panel arrow="top" header="Rizky Arifbudiman" key="2">
              <span className="group-list">SAM Group 1</span>
            </Panel>
          </Collapse>
        </div> */}
        <Menu defaultSelectedKeys={this.state.id} mode="inline">
          {this.state.collapsed ? (
            <div className="sidebar-divider">
              <Divider type="horizontal" />
            </div>
          ) : (
            <div className="sidebar-title-menu"></div>
          )}
          <Menu.Item key="1" className="sidebar-menu-list" onClick={e => this.onClickMenu(e)}>
            <NavLink to="/dashboard" />
            <Icon type="dashboard" style={{ marginLeft: "70px", fontSize: 30 }} />
            {/* <span>Dashboard</span> */}
          </Menu.Item>
          <Menu.Item key="2" className="sidebar-menu-list" onClick={e => this.onClickMenu(e)}>
            <NavLink to="/personal-dashboard" />
            <Icon type="snippets" style={{ marginLeft: "70px", fontSize: 30 }} />
            {/* <span>Personal Dashboard</span> */}
          </Menu.Item>
          {/* {this.state.collapsed ? (
            <div className="sidebar-divider">
              <Divider type="horizontal" />
            </div>
          ) : (
            <div className="sidebar-title-menu"></div>
          )} */}
          <Menu.Item key="3" className="sidebar-menu-list" onClick={e => this.onClickMenu(e)}>
            <NavLink to="/activity" />
            <Icon type="pie-chart" style={{ marginLeft: "70px", fontSize: 30 }} />
            {/* <span>Activity Monitoring</span> */}
          </Menu.Item>

          <Menu.Item key="4" className="sidebar-menu-list" onClick={e => this.onClickMenu(e)}>
            <NavLink to="/activity" />
            <Icon type="setting" style={{ marginLeft: "70px", fontSize: 30 }} />
            {/* <span>Activity Monitoring</span> */}
          </Menu.Item>
        </Menu>
      </Sider>
    );
  }
}

export default SidebarLayout;
