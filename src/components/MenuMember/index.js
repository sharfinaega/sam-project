import React, { Component } from 'react'
import { Col, Row, Icon, Input, Avatar } from 'antd'

class MenuMember extends Component {
    // constructor() {
    //     super()
    // }
    render() {
        const { dataMember = {}, memberListActive = {}, changeMember = () => { } } = this.props
        return (
            <Row>
                <Col className="text-center">
                    <span>Task member</span>
                </Col>
                <Col
                    className="text-center"
                    style={{ marginTop: "12px", marginBottom: "12px" }}
                >
                    <Input placeholder="Search member" />
                </Col>
                <Col>
                    <Row>
                        <Col className="modal-task-list-group">
                            {dataMember ? dataMember.map((data, index) => {
                                const arrName = data.user.name.split(" ");
                                let name = [];
                                name.push(arrName[0].substring(0, 1).toUpperCase());
                                if (arrName.length > 1) {
                                    name.push(arrName[1].substring(0, 1).toUpperCase());
                                }
                                return memberListActive[index] ? (
                                    <Col
                                        key={index}
                                        className="modal-task-list-item cursor-pointer"
                                        onClick={() => changeMember(data, index)}
                                        style={{ background: "#F7FBFF" }}
                                    >
                                        <section style={{ display: "flex" }}>
                                            <Col
                                                span={5}
                                                style={{ display: "flex", alignItems: "center" }}
                                            >
                                                <Avatar
                                                    style={{
                                                        color: "#f56a00",
                                                        backgroundColor: "#fde3cf"
                                                    }}
                                                    size="large"
                                                >
                                                    {name}
                                                </Avatar>
                                            </Col>
                                            <Col
                                                span={17}
                                                style={{ display: "flex", alignItems: "center" }}
                                            >
                                                <span style={{marginLeft: '8px'}}>{data.user.name}</span>
                                            </Col>
                                            <Col
                                                span={2}
                                                style={{ display: "flex", alignItems: "center" }}
                                            >
                                                <Icon type="check" />
                                            </Col>
                                        </section>
                                    </Col>
                                ) : (
                                        <Col
                                            key={index}
                                            className="modal-task-list-item cursor-pointer"
                                            onClick={() => changeMember(data, index)}
                                        >
                                            <section style={{ display: "flex" }}>
                                                <Col
                                                    span={5}
                                                    style={{ display: "flex", alignItems: "center" }}
                                                >
                                                    <Avatar
                                                        style={{
                                                            color: "#f56a00",
                                                            backgroundColor: "#fde3cf",
                                                        }}
                                                        size="large"
                                                    >
                                                        {name}
                                                    </Avatar>
                                                </Col>
                                                <Col
                                                    span={17}
                                                    style={{ display: "flex", alignItems: "center" }}
                                                >
                                                    <span style={{marginLeft: '8px'}}> {data.user.name}</span>
                                                </Col>
                                            </section>
                                        </Col>
                                    );
                            }) : null}
                        </Col>
                    </Row>
                </Col>
            </Row>
        )
    }
}

export default MenuMember