import React from 'react'
import Controller from './controller.js'
import Modal from '../Modal'
import { Upload, Row, Col, Icon, Typography, Menu, Dropdown, Progress, Button, DatePicker, Popover, Checkbox, Form, Avatar, Card, Popconfirm } from 'antd'
import Paragraph from 'antd/lib/typography/Paragraph'
import moment from 'moment'
import TextArea from 'antd/lib/input/TextArea'
import './style.scss'
import Activity from '../Activity/index.js'
import MenuMember from '../MenuMember/index.js'
import TaskMenuCheckList from './../TaskMenuChecklist'
import TaskMenuCheckListItem from './../TaskMenuChecklistItem'

class ModalTask extends Controller {
	constructor() {
		super()

	}
	render() {
		const { title = "", isOpen = false, onCancel = () => { }, data = [], dataMember = {}, memberListActive = {}, changeMember = () => { }, deleteCheck = () => { }, deleteCheckItem = () => { } } = this.props
		const { Dragger } = Upload;
		const props = {
			name: 'file',
			multiple: true,
			action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
			onChange(info) {
				const { status } = info.file;
				if (status !== 'uploading') {
					console.log(info.file, info.fileList);
				}
			},
		};
		moment.locale('id');
		const dateFormat = 'DD MMMM YYYY';
		const menuBadge = (
			<Menu>
				<Menu.Item key="0">
					<Col>
						<span className="modal-badge-item modal-badge-info cursor-pointer text-center">Not Started</span>
						<Icon type="check" className="modal-badge-item-check" />
					</Col>
				</Menu.Item>
				<Menu.Item key="1">
					<Col>
						<span className="modal-badge-item modal-badge-progress cursor-pointer text-center">In Progress</span>
					</Col>
				</Menu.Item>
				<Menu.Item key="2">
					<Col>
						<span className="modal-badge-item modal-badge-delayed cursor-pointer text-center">Delayed</span>
					</Col>
				</Menu.Item>
				<Menu.Item key="3">
					<Col>
						<span className="modal-badge-item modal-badge-done cursor-pointer text-center">Done</span>
					</Col>
				</Menu.Item>
				<Menu.Item key="4">
					<Col>
						<span className="modal-badge-item modal-badge-dropped cursor-pointer text-center">Dropped</span>
					</Col>
				</Menu.Item>
			</Menu>
		);
		return (
			<div>
				<Modal title={title} isOpen={isOpen} onCancel={onCancel} className="modal-task">
					<Row>
						<Col span={12} style={{ width: "70%" }}>
							<Row>
								<Col className="modal-task-header">
									<section className="modal-task-group">
										<Icon type="border" style={{ paddingTop: '4px' }} />
										<Col className="modal-task-detail-container">
											<Col className="modal-task-detail-title">
												<Paragraph editable={{ onChange: (e) => this.onChange(e, "titleTask") }} className="text-body" style={{ marginBottom: "2px" }} className="no-resize">
													{data.title}
												</Paragraph>
											</Col>
											<Col className="modal-task-detail-meta">
												<span>in <Typography.Text strong={true} className="text-primary">Open</Typography.Text> created by <Typography.Text strong={true} className="text-primary">MOCHAMAD SANTANA</Typography.Text>, 08 Agustus 2019</span>
											</Col>
											<Col span={6}>
												<Col span={16}>
													<span>Status</span>
													<Dropdown overlay={menuBadge} trigger={['click']}>
														<Row>
															<span className="modal-badge modal-badge-info cursor-pointer">Not Started</span>
															<Button type="link" icon="caret-down" />
														</Row>
													</Dropdown>
												</Col>
											</Col>
											<Col span={6} />
											<Col span={12}>
												<Col>
													<span>Due Date</span>
												</Col>
												<Col>
													<DatePicker format={dateFormat} onChange={this.onChangeDate} />
												</Col>
											</Col>
										</Col>
										<Col>
											<Popconfirm
												title="Are you sure delete this task?"
												onConfirm={(e) => this.handleDelete(e, "confirm")}
												onCancel={(e) => this.handleDelete(e, "cancel")}
												placement="bottom"
												okText="Delete"
												cancelText="Cancel"
											>
												<Button type="danger" icon="delete">Remove</Button>
											</Popconfirm>
										</Col>
									</section>
								</Col>
								<Col className="modal-task-body">
									<section className="modal-task-group">
										<Icon type="border" style={{ paddingTop: '4px' }} />
										<Col className="modal-task-detail-container">
											<Col className="modal-task-detail-title">
												<span style={{ marginBottom: "2px" }}>Description</span>
											</Col>
											<Col>
												{
													this.state.expandDescription ?
														<Row>
															<TextArea defaultValue={data.description} placeholder={"Add Description to this task.."} autoSize={{ minRows: 3, maxRows: 3 }} className="no-resize" onChange={(e) => this.onChange(e, "description")} />
															<Button icon="save" type={"primary"} style={{ marginTop: "6px" }} onClick={() => this.onSubmit("description")}>Save</Button>
														</Row>
														:
														<Button type="link" className="text-body" style={{ paddingLeft: "0px" }} onClick={(e) => this.onExpand("Description")}>{
															data.description ? data.description : "Add Description to this task.."
														}</Button>

												}
											</Col>
										</Col>
									</section>
									<section className="modal-task-group">
										<Icon type="border" style={{ paddingTop: '4px' }} />
										<Col className="modal-task-detail-container">
											<Col className="modal-task-detail-title" span={12}>
												<span style={{ marginBottom: "2px" }}>Checklist</span>
											</Col>
											<Col className="modal-task-detail-title text-right" span={12}>
												<Popover
													content={<TaskMenuCheckList onSubmitChecklist={this.onSubmitChecklist} />}
													trigger="click"
													visible={this.state.expandChecklist}
													onVisibleChange={() => this.onExpand("Checklist")}
												>
													<Button type="link" icon="plus">Add Checklist</Button>
												</Popover>
											</Col>
											<Col>
												{
													data.project_task_checklists === undefined || data.project_task_checklists.length === 0 ?
														<Col span={24}>
															<em>No checklist detected</em>
														</Col>
														:
														data.project_task_checklists.map((data, index) => {
															let checked = 0
															data.project_task_checklist_items.forEach((c) => {
																if (c.checked) {
																	return checked++
																}
																return true
															})
															const percent = (checked / data.project_task_checklist_items.length) * 100
															return (
																<Col key={index} span={24} style={{ marginBottom: 20 }}>
																	<Row>
																		<Col span={22}>
																			<Typography.Text delete={false}>{data.title}</Typography.Text>
																		</Col>
																		<Col span={2}>
																			<Button type="danger" size="small" onClick={() => deleteCheck(data.id)}>
																				<Icon type="delete" />
																			</Button>
																		</Col>
																	</Row>
																	<Progress percent={percent} status="active" />
																	<TaskMenuCheckListItem onSubmitChecklistItem={(title) => this.onSubmitChecklistItem(title, data.id, index)} />
																	{
																		data.project_task_checklist_items.map((item, idx) => {
																			return (
																				<div key={idx} style={{ marginBottom: 4 }}>
																					<Row>
																						<Col span={22}>
																							<Checkbox onChange={() => this.onCheckedChecklistItem(index, idx)} checked={item.checked} style={{ marginRight: 4 }} />
																							<Typography.Text delete={item.checked}>{item.title}</Typography.Text>
																						</Col>
																						<Col span={2}>
																							<Button type="danger" size="small" onClick={() => deleteCheckItem(item.id, item.project_task_checklist_id)}>
																								<Icon type="delete" />
																							</Button>
																						</Col>
																					</Row>
																				</div>
																			)
																		})
																	}
																</Col>
															)
														})

												}
											</Col>
										</Col>
									</section>
									<section className="modal-task-group">
										<Icon type="border" style={{ paddingTop: '4px' }} />
										<Col className="modal-task-detail-container">
											<Col span={24} className="modal-task-detail-title">
												<span style={{ marginBottom: "2px" }}>Attachments</span>
											</Col>
											<Col>
												<Dragger {...props}>
													<p className="ant-upload-drag-icon">
														<Icon type="inbox" />
													</p>
													<p className="ant-upload-text">Upload an attachment</p>
												</Dragger>
											</Col>
										</Col>
									</section>
								</Col>
								<Col className="modal-task-footer">
									<Col span={16}>
										{data.project_task_members && data.project_task_members.length > 0 && data.project_task_members.map((member, index) => {
											const arrName = member.user.name.split(" ")
											let name = []
											name.push(arrName[0].substring(0, 1).toUpperCase())
											if (arrName.length > 1) {
												name.push(arrName[1].substring(0, 1).toUpperCase())
											}
											return (
												<Avatar key={index} style={{ color: 'white', backgroundColor: '#0000FF', marginRight: '8px' }}>
													{name}
												</Avatar>
											);
										})}
									</Col>
									<Col span={8} className="text-right">
										<Popover
											content={<MenuMember dataMember={dataMember} memberListActive={memberListActive} changeMember={changeMember} />}
											trigger="click"
											visible={this.state.expandMember}
											onVisibleChange={() => this.onExpand("Member")}
										>
											<Button type="link" icon="plus">Add member</Button>
										</Popover>
									</Col>
								</Col>
							</Row>
						</Col>
						<Activity dataActivity={data.project_task_activities} receiveDataActivity={(data) => this.props.receiveDataActivity(data)} data={data} />
					</Row>
				</Modal>
			</div>
		)
	}
}

export default Form.create()(ModalTask)