import { Component } from 'react'
import TaskService from './../../services/project-task'
import TaskServiceActivity from './../../services/project-task-activity'
import moment from 'moment'
import { message } from 'antd'

class ModalTaskController extends Component {
    constructor() {
        super()
        this.state = {
            modalDelete: false,
            handleUpdateDescription: false,
            handleUpdateTitle: false,
            listCheck: [],
            titleChecklist: '',
            taskEnd: [],
            checkedList: false,
            description: '',
            expandDescription: false,
            expandChecklist: false,
            expandMember: false,
            memberListActive: [],
        }
    }
    handleDelete = async (e, type) => {
        if (type === "confirm") {
            try {
                await TaskService.deleteTask(this.props.data.id)
                message.success("Delete Success")
                window.location.reload();
            } catch (error) {
                console.log(error)
                message.error("Delete failed")
            }
        }
    }
    onChange = (e, state) => {
        if (state === "titleTask") {
            this.updateDataTitle(e)
        } else {
            this.setState({ [state]: e.target.value })
        }
    }
    onChangeDate = (e, dateString) => {
        this.updateDataDeadline(dateString)
    }

    onExpand = (type) => {
        if (type === "Description") {
            this.setState({ expandDescription: !this.state.expandDescription })
        } else if (type === "Checklist") {
            this.setState({ expandChecklist: !this.state.expandChecklist })
        } else if (type === "Member") {
            this.setState({ expandMember: !this.state.expandMember })
        }
    }

    onSubmit = async (type) => {
        if (type === "Checklist") {
            var { taskEnd, listCheck } = this.state
            listCheck.push(this.state.titleChecklist)
            taskEnd.push(false)
            this.setState({ titleChecklist: '', expandChecklist: false, taskEnd, listCheck })
            this.props.form.resetFields()
        } else if (type === "description") {
            try {
                await this.updateDataDescription(this.state.description)
                this.setState({expandDescription: false})
            } catch (error) {
                console.log(error)
            }
        }
    }

    handleClick = (type, index) => {
        if (type === "memberList") {
            let memberListActiveTemp = this.state.memberListActive.slice()
            memberListActiveTemp[index] = !memberListActiveTemp[index]
            this.setState({ memberListActive: memberListActiveTemp })
        }
    }

    updateDataTitle = async (title) => {
        this.props.updateDataTitle(title)
    }

    updateDataDescription = async (description) => {
        this.props.updateDataDescription(description)
    }

    updateDataDeadline = async (deadline) => {
        this.props.updateDataDeadline(deadline)
    }
    createDataActivity = async (payload) => {
        this.props.createDataActivity(payload)
    }
    onSubmitChecklist = async (title) => {
        const payload = {
            project_task_id: this.props.data.id,
            title: title
        }
        const resp = await TaskService.addProjectTaskChecklist(payload)
        if (resp.success) {
            this.setState(
                { expandChecklist: false }
            )
            this.props.onSubmitChecklist(resp.data)
            message.success('Checklist Added')
        }
    }
    onSubmitChecklistItem = async (title, checklist_id, index) => {
        const payload = {
            "project_task_checklist_id": checklist_id,
            "title": title,
            "checked": false
        }
        try {
            const resp = await TaskService.addProjectTaskChecklistItem(payload)
            if (resp.success) {
                message.success('Checklist item Added')
                this.props.onSubmitChecklistItem(resp.data, index)
            } else {

            }
        } catch (error) {
            console.log(error)
        }
    }
    onCheckedChecklistItem = async (indexChecklist, indexItem) => {
        const { data } = this.props
        const project_task_checklist = data.project_task_checklists[indexChecklist]
        const project_task_checklist_item = project_task_checklist.project_task_checklist_items[indexItem]
        try {
            const payload = {
                project_task_checklist_id: project_task_checklist.id,
                title: project_task_checklist_item.title,
                checked: !project_task_checklist_item.checked,
            }
            console.log(typeof payload)
            const response = await TaskService.updateChecklist(project_task_checklist_item.id, payload)
            if (response.success) {
                message.success('Checklist item changed')
                this.props.onCheckedChecklistItem(indexChecklist, indexItem)
            } else {
                message.error('Change item check failed')
            }
        } catch (error) {
            console.log(error)
        }
    }
}

export default ModalTaskController