import React, { Component } from "react";
import { Button } from "antd";
import "./style.scss";

class RenderLaneAdder extends Component {
  render() {
    const { addLane } = this.props;
    return (
      <div className="lane-adder-new">
        <Button
          type="primary"
          icon="plus"
          block
          onClick={() =>
            addLane(
              {
                id: 1,
                title: "PT Sentosa Indah",
                cards: [
                  {
                    id: 1,
                    title: "Teaser PT Sentosa Indah",
                    description: "Pengajuan Kredit Term 3"
                  }
                ]
              },
              {
                id: 2,
                title: "SAM 1 Monthly Outing Event",
                cards: [
                  {
                    id: 2,
                    title: "Konfirmasi: Alternatif tempat dan waktu",
                    description: "12 March 2020"
                  }
                ]
              },
              {
                id: 3,
                title: "Finish",
                cards: [
                  {
                    id: 3,
                    title: "Pt Nusa Indah",
                    description: "Pengajuan Kredit Term 1"
                  }
                ]
              }
            )
          }
        >
          Add a new step
        </Button>
      </div>
    );
  }
}

export default RenderLaneAdder;
