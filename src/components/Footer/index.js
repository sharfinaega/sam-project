import React, { Component } from "react";
import { Layout } from "antd";
const { Footer } = Layout;

class FooterLayout extends Component {
  render() {
    return <Footer>Footer</Footer>;
  }
}

export default FooterLayout;
