import React from "react";
import { Input, Button, Row, Col } from 'antd'
import Controller from './controller'
import "./style.scss"

class RenderLaneHeader extends Controller {
	render() {
		return (
			<div>
				<h5 className="kanban-title">{this.props.title}</h5>
				{!this.state.openForm ? (
					<Row className="kanban-new-button-wrapper">
						<Button
							type="primary"
							icon="plus"
							block
							onClick={() => this.toggleForm(true)}
						>
							Add New Task
						</Button>
					</Row>
				) : (
					<Row className="kanban-new-button-wrapper">
						<Col style={{ marginBottom: "8px" }}>
							<Input
								placeholder="Enter a title for this task"
								onChange={e => this.onChange(e, "cardName")}
								value={this.state.cardName}
							/>
						</Col>
						<Row>
							<Col span={20}>
								<Button
									className="kanban-submit-button"
									type="primary"
									icon="save"
									block
									disabled={ this.state.loading }
									onClick={this.addNewTask}
								>
									Submit Task
								</Button>
							</Col>
							<Col span={4}>
								<Button type="link" onClick={() => this.toggleForm(false)}>
									<span>x</span>
								</Button>
							</Col>
						</Row>
					</Row>
				)}
			</div>
		);
	}
}

export default RenderLaneHeader;
