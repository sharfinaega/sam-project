import { Component } from 'react';

class RenderLaneHeaderController extends Component {
	constructor(props){
		super(props)
		this.state = {
			openForm: false,
			cardName: '',
			loading: false
		}
	}
	toggleForm = (openForm) => {
		this.setState({ openForm })
	}
	onChange = (e, state) => {
		this.setState({ [state]: e.target.value });
	};
	addNewTask = async() => {
		this.setState({ loading: true })
		await this.props.onAddNewTask(this.state.cardName)
		this.setState({ cardName: '', loading: false, openForm: false })
	}
}

export default RenderLaneHeaderController;