import React from 'react';
import Controller from './controller'
import { Form, Col, Input, Button } from 'antd';
import "./style.scss"

class TaskMenuChecklistItem extends Controller {
	render() {
		const { getFieldDecorator } = this.props.form
		if (!this.state.toggleForm) {
			return (
				<div className="tmci-add-new" onClick={() => this.setState({ toggleForm: true })}>
					add checklist item...
				</div>
			)
		}
		return (
			<Form onSubmit={e => this.onSubmit(e)} style={{ marginBottom: 8 }}>
				<Form.Item className="no-error">
					{getFieldDecorator("titleChecklist", {
						rules: [{ required: true }]
					})(
						<Input placeholder="add checklist item..." />
					)}
				</Form.Item>
				<Col>
					<Button
						type="primary"
						htmlType="submit"
						size="small"
						style={{ marginTop: 8, marginRight: 8 }}
						onClick={e => this.onSubmit(e)}
					>
						Add
					</Button>
					<Button
						type="danger"
						size="small"
						style={{ marginTop: 8, }}
						onClick={() => this.setState({ toggleForm: false })}
					>
						Cancel
					</Button>
				</Col>
			</Form>
		);
	}
}

export default Form.create()(TaskMenuChecklistItem);