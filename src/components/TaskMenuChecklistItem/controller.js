import { Component } from 'react';

class TaskMenuChecklistItemController extends Component {
	constructor(props) {
		super(props)
		this.state = {
			toggleForm: false,
		}
	}
	onSubmit = (e) => {
		e.preventDefault()
		this.props.form.validateFields(async (err, values) => {
			if (!err) {
				this.props.onSubmitChecklistItem(values.titleChecklist)
				this.props.form.resetFields()
				this.setState({ toggleForm: false })
			}
		})
	}
}

export default TaskMenuChecklistItemController;