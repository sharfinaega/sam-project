import { Component } from "react";
import TaskServiceActivity from "./../../services/project-task-activity";
import { message } from "antd";

class ActivityController extends Component {
  constructor() {
    super();
    this.state = {
      comment: ""
    };
  }

  handleChange = (e, state) => {
    this.setState({ [state]: e.target.value });
  };

  handleClick = async type => {
    if (type === "comment") {
      const payloadActivity = {
        project_task_id: this.props.data.id,
        type: "comment",
        detail: this.state.comment
      };
      this.createDataActivity(payloadActivity);
    }
  };

  createDataActivity = async payload => {
    try {
      const response = await TaskServiceActivity.createProjectTaskActivity(payload);
      if (response.success) {
        this.props.receiveDataActivity(response.data);
        this.setState({ comment: "" });
      }
    } catch (error) {
      message.error("Error");
    }
  };
}

export default ActivityController;
