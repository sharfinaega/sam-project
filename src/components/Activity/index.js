import React from "react";
import Controller from "./controller.js";
import "./style.scss";
import { Row, Icon, Avatar, Card, Col, Checkbox, Button, Typography } from "antd";
import TextArea from "antd/lib/input/TextArea";
import moment from "moment";

class Activity extends Controller {
  constructor() {
    super();
  }
  render() {
    const { dataActivity = [], data } = this.props;
    return (
      <Col span={12} style={{ width: "30%", background: "#F7F9FC" }}>
        <Row>
          <Col className="modal-task-aside-header">
            <Icon type="flag" />
            <span className="modal-task-detail-title"> Activity</span>
          </Col>
          <Col className="modal-task-aside-body">
            {dataActivity.map(data => {
              return data.type === "edit" ? (
                <section className="modal-task-group">
                  <Avatar style={{ color: "#f56a00", backgroundColor: "#fde3cf" }}>U</Avatar>
                  <Col className="modal-task-detail-container">
                    <Col className="modal-activity-item-header">
                      <div>
                        <small>{`${JSON.parse(localStorage.getItem("user")).id === data.user.id ? "You" : data.user.name} ${data.type}ed this task`}</small>
                      </div>
                      <div>
                        <small>{moment(data.created_at).fromNow()}</small>
                      </div>
                    </Col>
                    <Col className="modal-task-activity">
                      <Card style={{ maxWidth: "293px" }}>
                        <Col>
                          <span>{data.detail}</span>
                        </Col>
                        <Col style={{ marginTop: "8px", marginBottom: "4px" }}>
                          {data.add_activity !== null && data.remove_activity === null ? (
                            <span className="modal-badge modal-badge-done">{data.add_activity}</span>
                          ) : data.remove_activity !== null && data.add_activity === null ? (
                            <span className="modal-badge modal-badge-dropped">{data.remove_activity}</span>
                          ) : (data.add_activity !== null) & (data.remove_activity !== null) ? (
                            <div>
                              <Col>
                                <Typography.Text delete className="modal-badge modal-badge-dropped">
                                  {data.remove_activity}
                                </Typography.Text>
                              </Col>
                              <Col style={{ marginTop: "10px" }}>
                                <span className="modal-badge modal-badge-done">{data.add_activity}</span>
                              </Col>
                            </div>
                          ) : null}
                        </Col>
                      </Card>
                    </Col>
                  </Col>
                </section>
              ) : data.type === "comment" ? (
                <section className="modal-task-group">
                  <Avatar style={{ color: "#f56a00", backgroundColor: "#fde3cf" }}>U</Avatar>
                  <Col className="modal-task-detail-container">
                    <Col className="modal-activity-item-header">
                      <div>
                        <small>{`${JSON.parse(localStorage.getItem("user")).id === data.user.id ? "You" : data.user.name} ${data.type}ed`}</small>
                      </div>
                      <div>
                        <small>{data.updated_at}</small>
                      </div>
                    </Col>
                    <Col className="modal-task-activity">
                      <Card>
                        <Col style={{ marginTop: "8px", marginBottom: "4px" }}>
                          <span>{data.detail}</span>
                        </Col>
                      </Card>
                    </Col>
                  </Col>
                </section>
              ) : null;
            })}
          </Col>
          <Col className="modal-task-aside-footer">
            <Col className="modal-task-detail-container">
              <Col className="modal-task-comment-container" style={{ background: "white" }}>
                <TextArea placeholder="Type your comment" autoSize={true} className="resize-none modal-task-textview" value={this.state.comment} onChange={e => this.handleChange(e, "comment")} />
                <Row>
                  {/* <Col span={18}>
                                        <Checkbox style={{ paddingLeft: '10px', paddingBottom: '8px' }}>Send when enter</Checkbox>
                                    </Col> */}
                  <Col span={24} style={{ textAlign: "right", paddingRight: "10px" }}>
                    <Button type="primary" style={{ marginBottom: 12 }} size="small" onClick={() => this.handleClick("comment")}>
                      Submit
                    </Button>
                  </Col>
                </Row>
              </Col>
            </Col>
          </Col>
        </Row>
      </Col>
    );
  }
}

export default Activity;
