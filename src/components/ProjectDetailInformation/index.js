import React, { Component } from 'react'
import { Col, Row, Typography, Tag } from 'antd'
import moment from 'moment'

class ProjectDetailInformation extends Component {
    // constructor() {
    //     super()
    // }
    render() {
        const { project = {} } = this.props
        let color = ''
        if (project.status === 'Not Started') {
            color = "#6990eb"
        } else if (project.status === 'In Progress') {
            color = "#2d95f3"
        } else if (project.status === 'Delayed') {
            color = "#ffd47a"
        } else if (project.status === 'Done') {
            color = "#04a777"
        } else if (project.status === 'Dropped') {
            color = "#ff486c"
        }
        return (
            <div>
                <Row>
                    <Col span={8} style={{ padding: '4px' }}>
                        Deskripsi
					</Col>
                    <Col span={16} style={{ padding: '4px' }}>
                        {project.description}
                    </Col>
                </Row>
                <Row>
                    <Col span={8} style={{ padding: '4px' }}>
                        Target Date
							</Col>
                    <Col span={16} style={{ padding: '4px' }}>
                        <Typography.Text type="danger">{moment(project.target_date).format('DD MMMM YYYY')}</Typography.Text>
                    </Col>
                </Row>
                <Row>
                    <Col span={8} style={{ padding: '4px' }}>
                        Status
							</Col>
                    <Col span={16} style={{ padding: '4px' }}>
                        <Tag color={color} style={{ width: '100%' }} align="middle">{project.status}</Tag>
                    </Col>
                </Row>
                <Row>
                    <Col span={8} style={{ padding: '4px' }}>
                        Product
					</Col>
                    <Col span={16} style={{ padding: '4px' }}>
                        {project.product}
                    </Col>
                </Row>
                <Row>
                    <Col span={8} style={{ padding: '4px' }}>
                        Customer
					</Col>
                    <Col span={16} style={{ padding: '4px' }}>
                        {project.customer}
                    </Col>
                </Row>
                <Row>
                    <Col span={8} style={{ padding: '4px' }}>
                        Amount
					</Col>
                    <Col span={16} style={{ padding: '4px' }}>
                        {project.amount ? `${project.amount}M` : '0'}
                    </Col>
                </Row>
            </div>
        )
    }
}

export default ProjectDetailInformation