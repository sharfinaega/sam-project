import React from "react";
import { Form, Col, Button, Input } from "antd";
import Controller from './controller'

class TaskMenuCheckList extends Controller {
	render() {
		const { getFieldDecorator } = this.props.form
		return (
			<Form onSubmit={e => this.onSubmit(e)}>
				<Col>
					<Col>
						<span>Name</span>
					</Col>
					<Form.Item className="no-error">
						{getFieldDecorator("titleChecklist", {
							rules: [{ required: true }]
						})(
							<Input />
						)}
					</Form.Item>
					<Col>
						<Button
							type="primary"
							htmlType="submit"
							style={{ marginTop: "6px" }}
							onClick={e => this.onSubmit(e, "Checklist")}
						>
							Submit
						</Button>
					</Col>
				</Col>
			</Form>
		);
	}
}

export default Form.create()(TaskMenuCheckList);
