import { Component } from 'react';

class TaskMenuChecklistController extends Component {
	constructor(props) {
		super(props)
		this.state = {
		}
	}

	onSubmit = (e) => {
		e.preventDefault()
		this.props.form.validateFields(async (err, values) => {
			if (!err) {
				this.props.onSubmitChecklist(values.titleChecklist)
				this.props.form.resetFields()
			}
		})
	}
}

export default TaskMenuChecklistController;