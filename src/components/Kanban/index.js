import React from "react";
import Board from "@lourenci/react-kanban";
import Controller from "./controller.js";
import ModalTask from "../ModalTask";
import RenderLaneHeader from "../RenderLaneHeader";
import RenderCardCanban from "../RenderCardKanban";
import RenderLaneAdder from "../RenderLaneAdder";
import "./style.scss";

class Kanban extends Controller {
  // constructor() {
  // 	super();
  // }
  render() {
    if (this.state.displayKanban) {
      return (
        <div>
          <Board
            allowAddLane
            disableLaneDrag
            onLaneNew={console.log}
            onCardNew={() => console.log("ada yg baru nih")}
            onCardDragEnd={(board, source, destination) =>
              this.onCardDragEnd(board, source, destination)
            }
            renderLaneAdder={({ addLane }) => (
              <RenderLaneAdder addLane={addLane} />
            )}
            renderLaneHeader={({ id, title }, { addCard }) => (
              <RenderLaneHeader
                title={title}
                onAddNewTask={name => this.addNewTask(id, name, addCard)}
              />
            )}
            renderCard={data => (
              <RenderCardCanban
                data={data}
                onOpen={data => this.onOpen(data)}
              />
            )}
          >
            {this.state.board}
          </Board>
          <ModalTask
            isOpen={this.state.isOpen}
            onCancel={() => this.onOpen()}
            projectId={this.props.projectId}
            data={this.state.dataTask}
            receiveDataActivity={data => this.receiveDataActivity(data)}
            dataMember={this.state.dataMember}
            onSubmitChecklist={data => this.onSubmitChecklist(data)}
            onSubmitChecklistItem={(data, index) =>
              this.onSubmitChecklistItem(data, index)
            }
            onCheckedChecklistItem={(indexChecklist, indexItem) =>
              this.onCheckedChecklistItem(indexChecklist, indexItem)
            }
            memberListActive={this.state.listCheckListMember}
            changeMember={(data, index) => this.handleClickMember(data, index)}
            updateDataTitle={title => this.updateDataTitle(title)}
            updateDataDescription={description =>
              this.updateDataDescription(description)
            }
            updateDataDeadline={deadline => this.updateDataDeadline(deadline)}
            createDataActivity={payload => this.createDataActivity(payload)}
            deleteCheck={id => this.deleteCheck(id)}
            deleteCheckItem={(id_item, id_cheklist) =>
              this.deleteCheckItem(id_item, id_cheklist)
            }
          />
        </div>
      );
    }
    return true;
  }
}

export default Kanban;
