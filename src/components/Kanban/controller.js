import { Component } from "react";
import taskService from "./../../services/project-task";
import { message } from "antd";
import moment from "moment";
import TaskServiceActivity from "./../../services/project-task-activity";

class KanbanController extends Component {
  constructor() {
    super();
    this.state = {
      isOpen: false,
      displayKanban: false,
      board: {
        lanes: []
      },
      dataTask: {},
      dataMember: [],
      listCheckListMember: []
    };
  }
  onChange = (e, state) => {
    this.setState({ [state]: e.target.value });
  };
  onOpen = data => {
    if (this.state.isOpen === false) {
      this.getTask(data);
      this.setState({ isOpen: true });
    } else {
      this.setState({ isOpen: false });
    }
  };

  getTask = async data => {
    await this.getDataTask(data.id);
    await this.getUser();
    this.compareMember();
  };
  compareMember = () => {
    this.state.dataTask.project_task_members.map(data => {
      var check = this.state.dataMember.findIndex(d => d.user_id === data.user_id);
      if (check >= 0) {
        return this.state.listCheckListMember.splice(check, 1, true);
      }
      return null;
    });
  };
  getDataTask = async id => {
    const resp = await taskService.getTaskById(id);
    if (resp.success) {
      this.setState({ dataTask: resp.data });
    }
  };
  componentDidMount() {
    let lanes = [];
    this.setState({ displayKanban: false });
    this.props.steps.forEach(step => {
      lanes.push({
        id: step.id,
        title: step.name,
        cards: step.project_tasks
      });
    });
    this.setState({
      board: {
        lanes
      },
      displayKanban: true
    });
  }

  handleClickMember = async (data, index) => {
    var member = this.state.dataTask.project_task_members;
    let memberListActiveTemp = this.state.listCheckListMember;
    memberListActiveTemp[index] = !memberListActiveTemp[index];
    if (memberListActiveTemp[index]) {
      const payload = {
        user_id: data.user_id,
        project_task_id: this.state.dataTask.id
      };
      const resp = await taskService.addTaskMember(payload);
      if (resp.success) {
        member.push(resp.data);
        this.props.handleClickMember(resp.data.project_task.id, resp.data.project_task.project_step_id, member);
        message.success("Add member success");
        this.setState({
          dataTask: {
            ...this.state.dataTask,
            project_task_members: member
          }
        });
      }
    } else {
      var i = this.state.dataTask.project_task_members.findIndex(d => d.user_id === data.user_id);
      const resp = await taskService.deleteTaskMember(this.state.dataTask.project_task_members[i].id);
      if (resp.success) {
        member.splice(i, 1);
        this.props.handleClickMember(resp.data.project_task_id, this.state.dataTask.project_step_id, member);
        this.setState({
          dataTask: {
            ...this.state.dataTask,
            project_task_members: member
          }
        });
        this.props.removeTaskMember(this.state.dataTask);
        message.success("Delete member success");
      }
    }
    this.setState({ listCheckListMember: memberListActiveTemp });
  };

  getUser = async () => {
    var listCheckListMember = [];
    this.props.projectMember.map(d => {
      return listCheckListMember.push(false);
    });
    this.setState({ dataMember: this.props.projectMember, listCheckListMember });
  };

  onCardDragEnd = async (board, source, destination) => {
    if (source.laneId !== destination.laneId) {
      const selectedBoard = board.lanes.find(b => {
        return b.id === destination.laneId;
      });
      const data = selectedBoard.cards[destination.index];
      const payload = {
        project_step_id: destination.laneId,
        title: data.title,
        description: data.description,
        deadline: data.deadline
      };
      await taskService.updateTask(data.id, payload);
    }
  };
  addNewTask = async (id, name, addCard) => {
    if (name.trim() !== "") {
      const payload = {
        project_step_id: id,
        project_id: this.props.projectId,
        title: name
      };
      const response = await taskService.createTask(payload);
      if (response.success) {
        addCard({
          id: response.data.id,
          title: response.data.title,
          data: {
            project_task_members: []
          }
        });
      }
    }
  };

  onSubmitChecklist = data => {
    let dataTask = this.state.dataTask;
    dataTask.project_task_checklists.push(data);
    this.setState({ dataTask });
  };

  onSubmitChecklistItem = (data, indexCheckList) => {
    let dataTask = this.state.dataTask;
    dataTask.project_task_checklists[indexCheckList].project_task_checklist_items.push({
      ...data,
      checked: false
    });
    this.setState({ dataTask });
  };

  onCheckedChecklistItem = (indexCheckList, indexItem) => {
    let dataTask = this.state.dataTask;
    dataTask.project_task_checklists[indexCheckList].project_task_checklist_items[indexItem].checked = !dataTask.project_task_checklists[indexCheckList].project_task_checklist_items[indexItem].checked;
    this.setState({ dataTask });
  };

  updateDataTitle = async title => {
    const payloadTitle = {
      title: title,
      project_step_id: this.state.dataTask.project_step_id,
      description: this.state.dataTask.description,
      deadline: this.state.dataTask.deadline,
      status: this.state.dataTask.status
    };
    const payloadActivity = {
      project_task_id: this.state.dataTask.id,
      type: "edit",
      detail: "Title changed",
      add_activity: title,
      remove_activity: title
    };
    try {
      const resp = await taskService.updateTask(this.state.dataTask.id, payloadTitle);
      if (resp.success) {
        this.props.updateDataTitle(resp.data.id, resp.data.project_step_id, resp.data.title);
        this.createDataActivity(payloadActivity);
        this.setState({
          dataTask: {
            ...this.state.dataTask,
            title: resp.data.title
          }
        });
        message.success("Title Changed");
      }
    } catch (error) {
      console.error(error);
    }
  };

  updateDataDescription = async description => {
    const payloadDescription = {
      title: this.state.dataTask.title,
      project_step_id: this.state.dataTask.project_step_id,
      description: description,
      deadline: this.state.dataTask.deadline,
      status: this.state.dataTask.status
    };
    const payloadActivity = {
      project_task_id: this.state.dataTask.id,
      type: "edit",
      detail: "Description changed",
      add_activity: description,
      remove_activity: description
    };
    try {
      const resp = await taskService.updateTask(this.state.dataTask.id, payloadDescription);
      if (resp.success) {
        this.createDataActivity(payloadActivity);
        this.setState({
          dataTask: {
            ...this.state.dataTask,
            description: resp.data.description
          }
        });
        message.success("Description Changed");
      }
    } catch (error) {
      console.error(error);
    }
  };

  updateDataDeadline = async deadline => {
    const deadlineToDate = moment(deadline).format("YYYY-MM-DD");
    const payloadDeadline = {
      title: this.state.dataTask.title,
      project_step_id: this.state.dataTask.project_step_id,
      description: this.state.dataTask.description,
      deadline: deadlineToDate,
      status: this.state.dataTask.status
    };
    const payloadActivity = {
      project_task_id: this.state.dataTask.id,
      type: "edit",
      detail: "Deadline changed",
      add_activity: deadline
    };
    try {
      const resp = await taskService.updateTask(this.state.dataTask.id, payloadDeadline);
      if (resp.success) {
        this.props.updateDataDeadline(resp.data.id, resp.data.project_step_id, resp.data.deadline);
        this.createDataActivity(payloadActivity);
        this.setState({
          dataTask: {
            ...this.state.dataTask,
            deadline: resp.data.deadline
          }
        });
        message.success("Deadline Changed");
      }
    } catch (error) {
      console.error(error);
    }
  };

  createDataActivity = payload => {
    TaskServiceActivity.createProjectTaskActivity(payload);
  };

  deleteCheck = async id => {
    var { dataTask } = this.state;
    var dataChecklist = dataTask.project_task_checklists;
    var index = dataChecklist.findIndex(d => d.id === id);
    const resp = await taskService.deleteProjectTaskChecklist(id);
    if (resp.success) {
      dataChecklist.splice(index, 1);
      this.setState({
        dataTask: {
          ...dataTask,
          project_task_checklists: dataChecklist
        }
      });
      message.info("Delete Success");
    }
  };

  deleteCheckItem = async (id_item, id_checklist) => {
    var { dataTask } = this.state;
    var project_task_checklists = dataTask.project_task_checklists;
    var indexCheckList = project_task_checklists.findIndex(d => d.id === id_checklist);
    var dataChecklistItem = project_task_checklists[indexCheckList].project_task_checklist_items;
    var indexItem = dataChecklistItem.findIndex(d => d.id === id_item);
    const resp = await taskService.deleteProjectTaskChecklistItem(id_item);

    if (resp.success) {
      dataChecklistItem.splice(indexItem, 1);
      project_task_checklists[indexCheckList].project_task_checklist_items = dataChecklistItem;
      this.setState({
        dataTask: {
          ...dataTask,
          project_task_checklists: [...project_task_checklists]
        }
      });
      message.info("Delete Success");
    }
  };

  receiveDataActivity = data => {
    let dataTask = this.state.dataTask;
    dataTask.project_task_activities.push(data);
    this.setState({ dataTask });
  };
}

export default KanbanController;
