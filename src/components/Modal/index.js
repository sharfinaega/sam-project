import React, { Component } from "react";
import { Modal } from "antd";
import "./style.scss";

class ModalMain extends Component {
  // constructor() {
  //     super()
  // }
  render() {
    const { title = "", className = "", isOpen = false, onCancel = () => {} } = this.props;
    return (
      <div className={className}>
        <Modal title={title} visible={isOpen} onCancel={onCancel} centered footer={null} width={"90vw"} style={{ transformOrigin: "66px 209px" }} className={className}>
          {this.props.children}
        </Modal>
      </div>
    );
  }
}

export default ModalMain;
