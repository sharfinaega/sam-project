import React, { Component } from "react";
import { Row, Col, Form, Button, Input, Select, DatePicker } from "antd";
import moment from "moment";
class FormEditProject extends Component {
  render() {
    const { getFieldDecorator = () => {}, project = {}, handleSubmit = () => {}, onLoading = false } = this.props;

    return (
      <div>
        <Form onSubmit={e => handleSubmit(e)}>
          <Row type="flex" justify="space-around" align="middle">
            <Col span={8} style={{ padding: "4px" }} justify="center">
              Title
            </Col>
            <Col span={16} style={{ height: "40px" }}>
              <Form.Item>
                {getFieldDecorator("title", {
                  rules: [{ required: true }],
                  initialValue: project.name
                })(<Input placeholder="title" />)}
              </Form.Item>
            </Col>
          </Row>
          <Row type="flex" justify="space-around" align="middle">
            <Col span={8} style={{ padding: "4px" }}>
              Deskripsi
            </Col>
            <Col span={16} style={{ height: "40px" }}>
              <Form.Item>
                {getFieldDecorator("description", {
                  rules: [{ required: false }],
                  initialValue: project.description
                })(<Input placeholder="description" />)}
              </Form.Item>
            </Col>
          </Row>
          <Row type="flex" justify="space-around" align="middle">
            <Col span={8} style={{ padding: "4px" }}>
              Target Date
            </Col>
            <Col span={16} style={{ paddingTop: "4px", height: "40px" }}>
              {getFieldDecorator("target_date", {
                rules: [{ required: true }],
                initialValue: moment(project.target_date, "YYYY/MM/DD")
              })(<DatePicker style={{ width: "100%" }} />)}
            </Col>
          </Row>
          <Row type="flex" justify="space-around" align="middle">
            <Col span={8} style={{ padding: "4px" }}>
              Status
            </Col>
            <Col span={16} style={{ height: "40px" }}>
              {getFieldDecorator("status", {
                rules: [{ required: false }],
                initialValue: project.status
              })(
                <Select showSearch style={{ width: "100%", paddingTop: "6px" }} placeholder="Select a status" optionFilterProp="children" filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
                  <Select.Option value="Not Started">Not Started</Select.Option>
                  <Select.Option value="Ongoing">Ongoing</Select.Option>
                  <Select.Option value="Delayed">Delayed</Select.Option>
                  <Select.Option value="Done">Done</Select.Option>
                  <Select.Option value="Dropped">Dropped</Select.Option>
                </Select>
              )}
            </Col>
          </Row>
          <Row type="flex" justify="space-around" align="middle">
            <Col span={8} style={{ padding: "4px" }}>
              Product
            </Col>
            <Col span={16} style={{ height: "40px", paddingTop: "4px" }}>
              <Form.Item>
                {getFieldDecorator("product", {
                  rules: [{ required: false }],
                  initialValue: project.product
                })(<Input placeholder="product" />)}
              </Form.Item>
            </Col>
          </Row>
          <Row type="flex" justify="space-around" align="middle">
            <Col span={8} style={{ padding: "4px" }}>
              Customer
            </Col>
            <Col span={16} style={{ height: "40px", paddingTop: "4px" }}>
              <Form.Item>
                {getFieldDecorator("customer", {
                  rules: [{ required: false }],
                  initialValue: project.customer
                })(<Input placeholder="customer" />)}
              </Form.Item>
            </Col>
          </Row>
          <Row type="flex" justify="space-around" align="middle">
            <Col span={8} style={{ padding: "4px" }}>
              Amount
            </Col>
            <Col span={16} style={{ height: "40px", paddingTop: "4px" }}>
              <Form.Item>
                {getFieldDecorator("amount", {
                  rules: [{ required: false }],
                  initialValue: project.amount
                })(<Input type="number" placeholder="Amount" />)}
              </Form.Item>
            </Col>
          </Row>
          <Row type="flex" justify="space-around" align="middle">
            <Button icon={`${onLoading ? "loading" : "save"}`} disable={onLoading.toString()} htmlType="submit" style={{ width: "100%", background: "#04a777", color: "white", fontStyle: "bold", marginTop: "1.5rem" }}>
              {onLoading ? null : "Save"}
            </Button>
          </Row>
        </Form>
      </div>
    );
  }
}

export default Form.create({ name: "create" })(FormEditProject);
