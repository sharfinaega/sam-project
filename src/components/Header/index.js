import React, { Component } from "react";
import { Layout, Row, Col, Icon, Button, Collapse, Avatar, Menu, Dropdown } from "antd";
import "./style.scss";

const { Header } = Layout;
const { Panel } = Collapse;

class HeaderLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: [],
      collapsed: false
    };
  }
  render() {
    const menu = (
      <Menu>
        <Menu.Item>
          <a target="_blank" rel="noopener noreferrer" href="http://www.alipay.com/">
            1st menu item
          </a>
        </Menu.Item>
        <Menu.Item>
          <a target="_blank" rel="noopener noreferrer" href="http://www.taobao.com/">
            2nd menu item
          </a>
        </Menu.Item>
        <Menu.Item>
          <a target="_blank" rel="noopener noreferrer" href="http://www.tmall.com/">
            3rd menu item
          </a>
        </Menu.Item>
      </Menu>
    );
    return (
      <Header className="header-container" justify="space-between">
        <Row type="flex" justify="space-between">
          <Col xs={6}>
            <h1 className="sidebar-sam">SAM Today</h1>
            {/* <Input size="large" placeholder="Search..." prefix={<Icon type="search" style={{ fontSize: 16, marginRight: 8 }} />} className="header-search-input" /> */}
          </Col>

          <Col>
            <Row type="flex">
              <Col className="header-icon-menu">
                <Button type="link" icon="setting" className="header-icon-menu-icon" />
              </Col>
              <Col className="header-icon-menu">
                <Button type="link" icon="bell" className="header-icon-menu-icon" />
              </Col>
              <Col className="header-icon-menu">
                <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" size={48} className="header-avatar" />
                <Dropdown overlay={menu}>
                  <a className="header-name-info">
                    Rizky Arief <Icon type="down" />
                  </a>
                </Dropdown>
              </Col>
            </Row>
          </Col>
        </Row>
      </Header>
    );
  }
}

export default HeaderLayout;
