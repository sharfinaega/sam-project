import React, { Component } from "react";
import { Col, Card, Row, Icon, Avatar, Button } from "antd";
import moment from "moment";
import "./style.scss";

class RenderCardCanban extends Component {
  render() {
    const { data, onOpen = () => {} } = this.props;
    return (
      <Card className="kanban-container" onClick={() => onOpen(data)}>
        <Col className="kanban-badge kanban-badge-info" />
        <Col className="kanban-body-title">
          <span>{data.title}</span>
        </Col>
        <Row className="kanban-avatar-stacked">
          <Col span={12}>
            {data.project_task_members &&
              data.project_task_members.length > 0 &&
              data.project_task_members.map((member, index) => {
                const arrName = member.user.name.split(" ");
                let name = [];
                name.push(arrName[0].substring(0, 1).toUpperCase());
                if (arrName.length > 1) {
                  name.push(arrName[1].substring(0, 1).toUpperCase());
                }
                return (
                  <div key="1" className="kanban-avatar" style={{ background: "blue" }}>
                    <span>ES</span>
                  </div>
                );
              })}
          </Col>
          <Col span={18} style={{ textAlign: "right" }}>
            <div className="kanban-task-badge kanban-badge-danger">
              {/* <Icon type="clock-circle" />
			  <span> {data.deadline ? moment(data.deadline).format("DD MMM YY") : "-"} </span>
			   */}
              <Avatar shape="square" key="1" style={{ color: "white", backgroundColor: "#0000FF", marginRight: "8px" }}>
                ES
              </Avatar>
              <Avatar shape="square" key="1" style={{ color: "white", backgroundColor: "#0000FF", marginRight: "8px" }}>
                RA
              </Avatar>
              <Avatar shape="square" key="1" style={{ color: "white", backgroundColor: "#0000FF", marginRight: "8px" }}>
                RP
              </Avatar>
              <Avatar shape="square" key="1" style={{ color: "grey", backgroundColor: "white", marginRight: "8px", border: "1px solid black" }}>
                +
              </Avatar>
            </div>
          </Col>
        </Row>
      </Card>
    );
  }
}

export default RenderCardCanban;
