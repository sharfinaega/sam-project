import React from "react";
import { Link } from "react-router-dom";
import { Breadcrumb } from "antd";
import "./style.scss";

class mBreadCrumb extends React.Component {
  render() {
    const { data = [] } = this.props;
    return (
      <div className="breadcrumb-container">
        {data.map((item, key) =>
          item.href ? (
            <Link key={key} to={item.href}>
              <Breadcrumb.Item separator=" / " className="breadcrumb-nonaktif">
                {item.title}
              </Breadcrumb.Item>
            </Link>
          ) : (
            <Breadcrumb.Item separator="" key={key} className="breadcrumb-aktif">
              {item.title}
            </Breadcrumb.Item>
          )
        )}
      </div>
    );
  }
}

export default mBreadCrumb;
