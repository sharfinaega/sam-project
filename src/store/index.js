import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'

import UserReducer from './../store/user/reducer'

const rootReducer = {
	userReducer: UserReducer
}

const reducer = combineReducers(rootReducer)
export const store = createStore(reducer, applyMiddleware(thunkMiddleware))