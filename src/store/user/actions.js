import * as constants from "./constants";
import authService from "./../../services/auth";
import history from "../../history";
import { setHtmlStorage } from "../../services/helper";

export function setLoading(loading) {
  return { type: constants.SET_LOADING, payload: loading };
}

export function setUser(user) {
  return { type: constants.SET_USER, payload: user };
}

export function login(username, password) {
  console.log(username, password);
  return async dispatch => {
    dispatch(setLoading(true));
    try {
      const response = await authService.login({
        username,
        password
      });
      console.log(response);
      dispatch(setUser(response.data));
      await setHtmlStorage("accessToken", response.access_token);

      history.push("/dashboard");
      return dispatch(setLoading(true));
    } catch (error) {
      console.log(error);
      return dispatch(setLoading(false));
    }
  };
}
