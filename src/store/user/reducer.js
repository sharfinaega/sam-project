import * as constants from './constants'

const INITIAL_STATE = {
	loading: false,
	user: {
		name: "",
		id: 0,
		username: "",
		email: "",
	}
}

export default (state = INITIAL_STATE, action) => {
	switch(action.type) {
	case constants.SET_LOADING:
		return Object.assign({}, state, {
			loading: action.payload
		})
	case constants.SET_USER:
		return Object.assign({}, state, {
			user : {
				name: action.payload.name ? action.payload.name : "",
				id: action.payload.id ? action.payload.id : "",
				username: action.payload.username ? action.payload.username : "",
				email: action.payload.email ? action.payload.email : "",
			}
		})
	default: return state
	}
}