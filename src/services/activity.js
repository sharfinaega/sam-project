import api from './api'

export default {
    getProjectType: () => api.get('project-type'),
    getProjects: (params = '') => api.get('project' + params),
    postProject: (payload) => api.post('project', null, { ...payload }),
    getDetailProject: (id) => api.get(`project/${id}`),
    updateProject: (id, payload) => api.put(`project/${id}`, { ...payload }),
    deleteProject: (id) => api.delete(`project/${id}`),
    deleteProjectMember: (id) => api.delete(`project-member/${id}`),
    addProjectMember: (payload) => api.post('project-member', { ...payload })
    // updateOrder: (id, payload) => api.put('order/' + id, null, { ...payload }),
    // deleteOrder: (id) => api.delete('order/' + id),
}