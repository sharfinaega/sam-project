import api from './api'

export default {
    createProjectTaskActivity: (payload) => api.post(`project-task-activity`, null, { ...payload }),
    updateProjectTaskActivity: (id, payload) => api.put(`project-task-activity/${id}`, null, { ...payload }),
    getProjectTaskActivity: () => api.get(`project-task-activity`),
    deleteProjectTaskActivity : (id) => api.delete(`project-task-activity/${id}`)
}