import api from './api'

export default {
    createTask: (payload) => api.post(`project-task`, null, { ...payload }),
    updateTask: (id, payload) => api.put(`project-task/${id}`, null, { ...payload }),
    getTask: () => api.get(`project-task`),
    getMyTask: () => api.get('mytask'),
    getTaskById: (id) => api.get(`project-task/${id}`),
    deleteTask: (id) => api.delete(`project-task/${id}`),
    addProjectTaskChecklist: (payload) => api.post('project-task-checklist', null, { ...payload }),
    deleteProjectTaskChecklist: (id) => api.delete(`project-task-checklist/${id}`),
    addProjectTaskChecklistItem: (payload) => api.post('project-task-checklist-item', null, { ...payload }),
    deleteProjectTaskChecklistItem: (id) => api.delete(`project-task-checklist-item/${id}`),
    getChecklist: () => api.get('project-task-checklist'),
    updateChecklist: (id, payload) => api.put(`project-task-checklist-item/${id}`, null, payload),
    addTaskMember: (payload) => api.post('project-task-member', null, { ...payload }),
    deleteTaskMember: (id) => api.delete(`project-task-member/${id}`),
}