import React, { Component } from "react";
import LayoutComponent from "./../layout";

export default (ComposedComponent, breadCrumb = []) => {
  return class Layout extends Component {
    render() {
      return (
        <LayoutComponent breadCrumb={breadCrumb}>
          <ComposedComponent match={this.props.match} />
        </LayoutComponent>
      );
    }
  };
};
