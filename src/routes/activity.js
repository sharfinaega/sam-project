import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import layout from "./layout";
import Async from "react-code-splitting";

const Activity = props => <Async load={import("./../pages/activity")} componentProps={props} />;
const DetailActivity = props => <Async load={import("./../pages/activity/detailactivity")} componentProps={props} />;

class ActivityRoutes extends Component {
  render() {
    // const { path } = this.props.match
    return (
      <Switch>
        <Route exact path="/activity" component={layout(Activity, [{ title: "ACTIVITY", href: "/activity" }, { title: "Activity Monitoring" }])} />
        <Route path="/activity/:id" component={layout(DetailActivity)} />
      </Switch>
    );
  }
}

export default ActivityRoutes;
