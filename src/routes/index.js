import React, { Component } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import Async from "react-code-splitting";
import layout from "./layout";

const Login = props => <Async load={import("../pages/login")} componentProps={props} />;
const Dashboard = props => <Async load={import("./../pages/dashboard")} componentProps={props} />;
const activity = props => <Async load={import("./activity")} componentProps={props} />;

class Routes extends Component {
  render() {
    return (
      <Switch>
        <Route
          exact
          path="/"
          component={() => {
            return <Redirect to="/login" />;
          }}
        />
        <Route path="/login" component={Login} />
        {/* <Route path="/dashboard" component={layout(Dashboard, [{ title: "MY PAGE", href: "/dashboard" }, { title: "Dashboard" }])} /> */}
        <Route path="/dashboard" component={layout(Dashboard, [{ title: "", href: "/dashboard" }, { title: "" }])} />
        <Route path="/activity" component={activity} />
      </Switch>
    );
  }
}

export default Routes;
