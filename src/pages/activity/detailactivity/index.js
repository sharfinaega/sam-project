import React from "react";
import { Col, Row, Drawer, Button, Avatar, Typography, Form, Progress, Popover, Select } from "antd";
import Kanban from "../../../components/Kanban";
import ProjectDetailInformation from "../../../components/ProjectDetailInformation";
import FormEditProject from "../../../components/FormEditProject";
// import mandiri from "../../../images/mandiri.svg";
import Controller from "./controller.js";
import "./style.scss";

class DetailActivity extends Controller {
  // constructor() {
  // 	super()
  // }
  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <div className="kanban">
        <Row type="flex" justify="space-around" align="middle">
          <Col span={18} style={{ paddingLeft: "14px" }}>
            <Typography.Title level={2}>{this.state.project.name}</Typography.Title>
          </Col>
          <Col span={6} style={{ paddingLeft: "14px" }} className="text-right">
            <Button icon="info-circle" type="link" style={{ padding: "0px", margin: "0px" }} onClick={this.onOpenDrawer}>
              <Typography.Text className="drawer-text-blue">Project Info</Typography.Text>
            </Button>
          </Col>
        </Row>

        <Row>
          <Col span={this.state.drawerOpen ? 15 : 24}>
            {this.state.project.project_steps && (
              <Kanban
                removeTaskMember={dataTask => this.removeTaskMember(dataTask)}
                steps={this.state.project.project_steps}
                projectId={this.props.match.params.id}
                projectMember={this.state.project.project_members}
                updateDataTitle={(id_task, id_step, title) => this.updateDataTitle(id_task, id_step, title)}
                updateDataDeadline={(id_task, id_step, deadline) => this.updateDataDeadline(id_task, id_step, deadline)}
                handleClickMember={(id_task, id_step, member) => this.handleClickMember(id_task, id_step, member)}
              />
            )}
          </Col>
          <Drawer visible={this.state.drawerOpen} mask={false} width={"400px"} onClose={this.onOpenDrawer} title={this.state.project.type}>
            <Col>
              <Row style={{ height: "20vh" }} type="flex" justify="space-around" align="middle">
                {/* <img src={mandiri} style={{ maxWidth: "20vw" }} alt="Logo Mandiri" /> */}
              </Row>
              <Row style={{ padding: "4px" }} type="flex" justify="space-around" align="middle">
                {this.state.onEdit ? null : <Typography.Text strong>{this.state.project.name}</Typography.Text>}
              </Row>
              <Row style={{ marginBottom: "4px" }}>
                <Col span={18}>
                  <Typography.Text strong className="drawer-text-blue">
                    More Information
                  </Typography.Text>
                </Col>
                <Col span={6} type="flex" align="end">
                  {this.state.onEdit ? null : (
                    <Button icon="edit" type="link" style={{ padding: "0px", margin: "0px" }} onClick={this.onEdit}>
                      <Typography.Text strong className="drawer-text-blue">
                        Edit
                      </Typography.Text>
                    </Button>
                  )}
                </Col>
              </Row>

              {this.state.onEdit ? <FormEditProject getFieldDecorator={getFieldDecorator} project={this.state.project} handleSubmit={e => this.handleSubmit(e)} onLoading={this.state.onLoading} /> : <ProjectDetailInformation project={this.state.project} />}

              <Row style={{ marginTop: "4px", marginBottom: "8px" }}>
                <Col span={20}>
                  <Typography.Text strong className="drawer-text-blue">
                    Members({this.state.project.project_members && this.state.project.project_members.length})
                  </Typography.Text>
                </Col>
                <Col span={4} type="flex" align="end">
                  <Button icon="edit" type="link" style={{ padding: "0px", margin: "0px" }} onClick={this.onEditMember}>
                    <Typography.Text strong className="drawer-text-blue">
                      Edit
                    </Typography.Text>
                  </Button>
                </Col>
              </Row>
              <Row style={{ padding: "4px" }} span={24}>
                {this.state.onEditMember ? (
                  <Popover
                    content={
                      <Row span={24} style={{ width: "20vw" }}>
                        <Form onSubmit={e => this.handleAddMember(e)}>
                          <Col span={18}>
                            <Form.Item>
                              {getFieldDecorator("member", {
                                rules: [{ required: true }]
                              })(
                                <Select placeholder="Select Member" style={{ width: "100%" }}>
                                  {this.state.users
                                    ? this.state.users.map((user, index) => {
                                        var check = this.state.project.project_members && this.state.project.project_members.findIndex(data => data.user_id === user.id);
                                        if (check === -1) {
                                          return (
                                            <Select.Option value={user.id} key={index}>
                                              {user.name}
                                            </Select.Option>
                                          );
                                        } else {
                                          return null;
                                        }
                                      })
                                    : null}
                                </Select>
                              )}
                            </Form.Item>
                          </Col>
                          <Col span={6}>
                            <Button type="primary" htmlType="submit" style={{ margin: "4px" }} icon={`${this.state.onLoading ? "loading" : null}`} disable={this.state.onLoading ? "true" : "false"}>
                              {this.state.onLoading ? null : "Add"}
                            </Button>
                          </Col>
                        </Form>
                      </Row>
                    }
                    title="Add Member"
                    trigger="click"
                    visible={this.state.visibleAddMember}
                    onVisibleChange={this.handleVisibleAddMember}
                    style={{ width: 400 }}
                  >
                    <Col span={4} style={{ cursor: "pointer" }}>
                      <Progress type="circle" width={40} percent={Number(`${this.state.project.project_members.length}0`)} format={percent => `+`} strokeColor={{ "0": "#555555" }} />
                    </Col>
                  </Popover>
                ) : null}
                {this.state.project.project_members &&
                  this.state.project.project_members.map((data, index) => {
                    let name = data.user.name;
                    let res = name.split(" ");
                    let awal = res[0] ? res[0].substring(0, 1).toUpperCase() : "";
                    let akhir = res[1] ? res[1].substring(0, 1).toUpperCase() : "";
                    return (
                      <Col span={4} key={index} style={{ marginBottom: 4 }}>
                        <Avatar className="drawer-avatar" size="large">
                          {awal + akhir}
                        </Avatar>
                        {this.state.onEditMember ? (
                          <Button type="danger" onClick={() => this.onDeleteMember(data.id, index)} className="drawer-button-delete-member">
                            x
                          </Button>
                        ) : null}
                      </Col>
                    );
                  })}
              </Row>
              <Row style={{ padding: "4px", marginTop: "48px" }}>
                <Button type="danger" icon={`${this.state.onLoading ? "loading" : "delete"}`} disable={this.state.onLoading.toString()} style={{ width: "100%" }} onClick={this.handleDelete}>
                  {this.state.onLoading ? null : "Delete Project"}
                </Button>
              </Row>
            </Col>
          </Drawer>
        </Row>
      </div>
    );
  }
}

export default Form.create({ name: "create" })(DetailActivity);
