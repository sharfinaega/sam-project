import { Component } from "react";
import activity from "../../../services/activity";
import user from "../../../services/user";
import moment from "moment";
import history from "../../../history";
import { message } from "antd";
class DetailActivityController extends Component {
  constructor() {
    super();
    this.state = {
      project: {},
      drawerOpen: true,
      onEdit: false,
      onEditMember: false,
      onLoading: false,
      visibleAddMember: false,
      users: {},
      selectedMember: null
    };
  }

  componentDidMount = async () => {
    this.getDataProject();
    this.getDataMember();
  };
  getDataProject = async () => {
    try {
      const resp = await activity.getDetailProject(this.props.match.params.id);
      if (resp.success) {
        this.setState({
          project: resp.data
        });
      }
    } catch (error) {
      console.log(error);
    }
  };

  getDataMember = async () => {
    try {
      const response = await user.getUsers();
      if (response.success) {
        this.setState({ users: response.data });
      }
    } catch (error) {
      console.error(error);
    }
  };

  onOpenDrawer = () => {
    this.setState({ drawerOpen: !this.state.drawerOpen });
  };

  onEdit = () => {
    this.setState({
      onEdit: true
    });
  };

  handleSubmit = async e => {
    e.preventDefault();
    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        let payload = {
          name: values.title,
          type: this.state.project.type,
          description: values.description,
          members: this.state.project.project_members,
          target_date: moment(values.target_date).format("YYYY-MM-DD"),
          status: values.status,
          product: values.product,
          customer: values.customer,
          amount: values.amount
        };
        this.setState({
          onLoading: true
        });
        try {
          const resp = await activity.updateProject(this.props.match.params.id, payload);
          if (resp.success) {
            message.success("Data Saved Successfully");
            this.setState({
              onLoading: false,
              onEdit: false,
              project: { ...this.state.project, ...resp.data }
            });
          }
        } catch (error) {
          message.success("Data Saved Failed ");
        }
      }
    });
  };
  handleDelete = async () => {
    this.setState({
      onLoading: true
    });
    const resp = await activity.deleteProject(this.props.match.params.id);
    if (resp.success) {
      message.success("Data Deleted Successfully");
      this.setState(
        {
          onLoading: false
        },
        () => {
          history.push("/activity");
        }
      );
    }
  };
  onEditMember = () => {
    this.setState({
      onEditMember: !this.state.onEditMember
    });
  };
  onDeleteMember = async (id, index) => {
    let project_members = this.state.project.project_members;
    try {
      const resp = await activity.deleteProjectMember(id);

      if (resp.success) {
        message.success("Member Deleted Successfully");
        project_members.splice(index, 1);
        this.setState({
          project: {
            ...this.state.project,
            project_members: project_members
          }
        });
      }
    } catch (error) {
      message.error("Error");
    }
  };

  handleVisibleAddMember = visibleAddMember => {
    this.setState({ visibleAddMember });
  };

  handleAddMember = async e => {
    e.preventDefault();
    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        this.setState({
          onLoading: true
        });
        let project_members = this.state.project.project_members;
        const payload = {
          user_id: values.member,
          project_id: Number(this.props.match.params.id)
        };
        try {
          const resp = await activity.addProjectMember(payload);
          if (resp.success) {
            this.props.form.resetFields();
            message.success("Member Added Successfully");
            project_members.push(resp.data);
            this.setState({
              project: {
                ...this.state.project,
                project_members: project_members
              },
              visibleAddMember: false,
              onLoading: false
            });
          }
        } catch (error) {
          message.error(error);
        }
      }
    });
  };

  onOpenDrawer = () => {
    this.setState({
      drawerOpen: !this.state.drawerOpen
    });
  };

  updateDataTitle = (id_task, id_step, title) => {
    var { project } = this.state;
    var project_steps = project.project_steps;
    var stepIndex = project_steps.findIndex(d => d.id === id_step);
    var project_tasks = project_steps[stepIndex].project_tasks;
    var taskIndex = project_tasks.findIndex(d => d.id === id_task);
    var updateTask = project_tasks[taskIndex];

    updateTask = {
      ...updateTask,
      title: title
    };

    project_tasks.splice(taskIndex, 1, updateTask);
    project = {
      ...project,
      project_steps: {
        ...project_steps,
        project_tasks: project_tasks
      }
    };
    this.setState({ project });
  };

  updateDataDeadline = (id_task, id_step, deadline) => {
    var { project } = this.state;
    var project_steps = project.project_steps;
    var stepIndex = project_steps.findIndex(d => d.id === id_step);
    var project_tasks = project_steps[stepIndex].project_tasks;
    var taskIndex = project_tasks.findIndex(d => d.id === id_task);
    var updateTask = project_tasks[taskIndex];

    updateTask = {
      ...updateTask,
      deadline: deadline
    };

    project_tasks.splice(taskIndex, 1, updateTask);
    project = {
      ...project,
      project_steps: {
        ...project_steps,
        project_tasks: project_tasks
      }
    };
    this.setState({ project });
  };

  handleClickMember = (id_task, id_step, member) => {
    let project = this.state.project;
    var project_steps = project.project_steps;
    var stepIndex = project_steps.findIndex(d => d.id === id_step);
    var project_tasks = project_steps[stepIndex].project_tasks;
    var taskIndex = project_tasks.findIndex(d => d.id === id_task);
    var updateTask = project_tasks[taskIndex];

    updateTask = {
      ...updateTask,
      project_task_members: member
    };

    project_tasks.splice(taskIndex, 1, updateTask);

    project_steps[stepIndex].project_tasks = project_tasks;

    project = {
      ...project,
      project_steps: [...project_steps]
    };
    this.setState({ project });
  };
  removeTaskMember = dataTask => {
    // let project = this.state.project
    // const indexStep = project.project_steps.findIndex((d) => d.id = dataTask.project_step_id)
    // const indexTask = project.project_steps[indexStep].project_tasks.findIndex((d) => d.id ===  dataTask.id)
    // console.log("sini", dataTask, indexStep, indexTask)
    // project.project_steps[indexStep].data_tasks[indexTask] = dataTask
    // console.log("sini2", project.project_steps[indexStep].data_tasks[indexTask])
    // this.setState({ project })
  };
}

export default DetailActivityController;
