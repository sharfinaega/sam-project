import React from "react";
import ActivityService from "./../../services/activity";
import ProjectTaskService from "./../../services/project-task";
import UserService from "./../../services/user";
import { message } from "antd";
import moment from "moment";
import history from "../../history";

class ActivityController extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loadingTable: false,
      loadingType: false,
      loadingMember: false,
      loadingCreate: false,
      loadingMyTask: false,
      showFilter: true,
      filterType: "",
      filterDeadline: "",
      visibleModalCreate: false,
      dataProjects: [],
      dataMembers: [],
      dataMyTask: [],
      projectType: [],
      projectPage: {},
      totalPage: 0,
      page: 1,
      currentPage: 1,
      perPage: 10
    };
  }

  componentDidMount() {
    this.getProjectType();
    this.getDataProjects();
    this.getDataMembers();
    this.getMytask();
  }

  getProjectType = async () => {
    this.setState({ loadingType: true });
    try {
      const response = await ActivityService.getProjectType();
      if (response.success) {
        this.setState({ projectType: response.data });
      }
    } catch (error) {
      console.error(error);
    }
    this.setState({ loadingType: false });
  };

  getDataProjects = async () => {
    this.setState({ loadingTable: true });
    try {
      // [["target_date","<","2020-10-10"]]
      let filter = [];
      let page = `?paginate=${this.state.perPage}&page=${this.state.page}`;
      if (this.state.filterType) {
        filter.push(["type", "=", this.state.filterType]);
      }
      if (this.state.filterDeadline) {
        filter.push(["target_date", "<=", this.state.filterDeadline]);
      }
      let params = filter.length > 0 ? page + "&where=" + JSON.stringify(filter) : page;
      const response = await ActivityService.getProjects(params);
      if (response.success) {
        this.setState({
          dataProjects: response.data,
          totalPage: response.page.total,
          projectPage: response.page
        });
      }
    } catch (error) {
      console.error(error);
    }
    this.setState({ loadingTable: false });
  };

  handleChangePagination = value => {
    this.setState(
      {
        currentPage: value,
        page: value
      },
      this.getDataProjects
    );
  };

  onChangeType = e => {
    this.setState(
      {
        filterType: e
      },
      this.getDataProjects
    );
  };

  onChangeDeadline = e => {
    this.setState(
      {
        filterDeadline: e ? moment(e).format("YYYY-MM-DD") : ""
      },
      this.getDataProjects
    );
  };

  getDataMembers = async () => {
    this.setState({ loadingMember: true });
    try {
      const response = await UserService.getUsers();
      if (response.success) {
        this.setState({ dataMembers: response.data });
      }
    } catch (error) {
      console.error(error);
    }
    this.setState({ loadingMember: false });
  };

  showModalCreate = () => {
    this.setState({
      visibleModalCreate: !this.state.visibleModalCreate
    });
  };

  handleSubmit = async e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.createProject(values);
      }
    });
  };

  createProject = async values => {
    this.setState({ loadingCreate: true });
    try {
      let payload = {
        name: values.name,
        type: values.type,
        target_date: moment(values.target_date).format("YYYY-MM-DD")
      };
      if (values.description) {
        payload = {
          ...payload,
          description: values.description
        };
      }
      if (values.members) {
        payload = {
          ...payload,
          members: values.members
        };
      }
      const response = await ActivityService.postProject(payload);
      if (response.success) {
        this.setState({ visibleModalCreate: false });
        this.getDataProjects();
        message.success("Success! Your new data has been added.");
      }
    } catch (error) {
      console.error(error);
    }
    this.setState({ loadingCreate: false });
  };

  onClickRow = data => {
    history.push(`/activity/${data.id}`);
  };

  getMytask = async () => {
    this.setState({ loadingMyTask: true });
    try {
      const response = await ProjectTaskService.getMyTask();
      if (response.success) {
        this.setState({ dataMyTask: response.data });
      }
    } catch (error) {
      console.error(error);
    }
    this.setState({ loadingMyTask: false });
  };

  onFilter = () => {
    this.setState({ showFilter: !this.state.showFilter });
  };
}

export default ActivityController;
