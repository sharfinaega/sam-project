import React from "react";
import Controller from "./controller";
import { Row, Col, Button, Card, Select, DatePicker, Table, Tag, Icon, Modal, Form, Input, Avatar, Tooltip, Pagination, Spin } from "antd";
import moment from "moment";
import "./style.scss";

const { Option } = Select;

const data = [
  {
    key: "1",
    name: "Test Project",
    type: "Financial",
    status: "Done",
    members: "ES",
    deadline: "",
    totalTask: "5"
  },
  {
    key: "2",
    name: "Sam Project",
    type: "Financial",
    status: "In Progress",
    members: "ES",
    deadline: "11 Januari 2020",
    totalTask: "3"
  },
  {
    key: "3",
    name: "Try Project",
    type: "Non Financial",
    status: "Delayed",
    members: "RB",
    deadline: "21 Januari 2020",
    totalTask: "6"
  }
];
class Activity extends Controller {
  render() {
    const { getFieldDecorator } = this.props.form;
    const colorMember = ["activity-color1", "activity-color2", "activity-color3", "activity-color4"];
    const columns = [
      {
        title: "Title",
        dataIndex: "name",
        key: "name"
      },
      {
        title: "Type",
        dataIndex: "type",
        key: "type"
      },
      {
        title: "Status",
        key: "status",
        render: (text, record) => {
          let color = "";
          if (record.status === "Not Started") {
            color = "#6990eb";
          } else if (record.status === "In Progress") {
            color = "#2d95f3";
          } else if (record.status === "Delayed") {
            color = "#ffd47a";
          } else if (record.status === "Done") {
            color = "#04a777";
          } else if (record.status === "Dropped") {
            color = "#ff486c";
          }
          return <div>{record.status ? <Tag color={color}>{record.status}</Tag> : "-"}</div>;
        }
      },
      {
        title: "Members",
        key: "members",
        render: (text, record) => (
          <div>
            {/* {
							record.project_members.length > 0 ?
								<span className="activity-table-member-container"> */}
            {
              // record.project_members.map((item, key) => {
              // 	let name = item.user.name
              // 	let res = name.split(' ')
              // 	let awal = res[0] ? res[0].substring(0, 1).toUpperCase() : ''
              // 	let akhir = res[1] ? res[1].substring(0, 1).toUpperCase() : ''
              // 	if (key < 3) {
              // 		return (
              <Tooltip placement="bottomLeft" title="name" key="1">
                <Avatar className="activity-table-member" style={{ marginLeft: "-16px", backgroundColor: "#0000FF" }}>
                  ES
                </Avatar>
                <Avatar className="activity-table-member" style={{ marginLeft: "-16px", backgroundColor: "maroon" }}>
                  RB
                </Avatar>
              </Tooltip>
              // 		)
              // 	}else{
              // 		return true
              // 	}
              // })
            }
            {/* {
										record.project_members.length > 3 ?
											<Avatar className="activity-table-member-number" style={{ marginLeft: '-16px' }}>
												+{(record.project_members.length - 3)}
											</Avatar>
											: null
									}
								</span>
								:
								'-'
						} */}
          </div>
        )
      },
      {
        title: "Deadline",
        key: "target_date",
        render: (text, record) => (
          <div>
            <span>
              <Icon type="clock-circle" />
              {" 11 Januari 2020"}
            </span>
          </div>
        )
      },
      {
        title: "Total Tasks",
        key: "project_tasks",
        render: (text, record) => (
          <div>
            <span>
              <Icon type="project" />
              {data[1].totalTask}
            </span>
          </div>
        )
      }
    ];

    return (
      <Row>
        <Row type="flex" justify="space-between" align="middle" className="activity-card-filter">
          <Col xs={6} className="activity-title">
            Activity Monitoring
          </Col>
          <Col xs={10} className="activity-btn-wrap-top">
            <Button type="primary" icon="plus" onClick={this.showModalCreate}>
              Create Project
            </Button>
            <Button type="primary" icon="filter" className="btn-filter" onClick={this.onFilter}>
              Filters
            </Button>
          </Col>
        </Row>
        <div className={"activity-filter ".concat(this.state.showFilter ? "" : "activity-no-filter")}>
          <Card className="activity-card">
            <Row type="flex" gutter={16}>
              <Col xs={6}>
                <span className="activity-title-form-input">Type</span>
                <Select placeholder="Type" className="activity-full-width" loading={this.state.loadingType} onChange={e => this.onChangeType(e)}>
                  <Option key="1" value="Financial">
                    Financial
                  </Option>
                  <Option key="2" value="Non Financial">
                    Non Financial
                  </Option>
                </Select>
              </Col>
              <Col xs={6}>
                <span className="activity-title-form-input">Deadline</span>
                <DatePicker className="activity-full-width" placeholder="Choose a date" format="DD MMMM YYYY" onChange={e => this.onChangeDeadline(e)} />
              </Col>
            </Row>
          </Card>
        </div>
        <Row gutter={16}>
          <Col xs={16}>
            <Table
              className="activity-table"
              //   dataSource={this.state.dataProjects}
              dataSource={data}
              columns={columns}
              pagination={false}
              rowKey="id"
              loading={this.state.loadingTable}
              onRow={(record, rowIndex) => {
                return {
                  onClick: () => this.onClickRow(record)
                };
              }}
            />
            {this.state.dataProjects.length > 0 ? (
              <Row className="activity-wrap-pagination">
                <Pagination total={this.state.totalPage} defaultCurrent={this.state.currentPage} defaultPageSize={this.state.perPage} onChange={this.handleChangePagination} />
              </Row>
            ) : null}
          </Col>
          <Col xs={8}>
            <Card className="activity-card">
              <Row type="flex" justify="space-between" align="middle">
                <Col xs={12}>
                  <span className="activity-title-task">Your Task ({this.state.dataMyTask.length})</span>
                </Col>
                <Col xs={12} className="activity-right-item">
                  <span className="activity-text-link">See more</span>
                </Col>
              </Row>
              <Row className="card-body">
                <Spin tip="Loading..." spinning={this.state.loadingMyTask} style={{ width: "100%" }}>
                  {this.state.dataMyTask.length > 0
                    ? this.state.dataMyTask.map((item, key) => {
                        return (
                          <Row key={key} type="flex" align="middle" className="activity-task-container">
                            <div className="activity-badge activity-status-not-started" />
                            <Row className="activity-task-content">
                              <p className="activity-task-title">{item.project_task.title}</p>
                              {/* <div className="activity-task-tag">{item.project_task.project.name}</div> */}
                            </Row>
                          </Row>
                        );
                      })
                    : null}
                </Spin>
              </Row>
            </Card>
          </Col>
        </Row>
        <Modal title="Create Project" visible={this.state.visibleModalCreate} centered onCancel={this.showModalCreate} footer={false} className="activity-modal-container">
          {this.state.visibleModalCreate ? (
            <Form onSubmit={e => this.handleSubmit(e)}>
              <Row>
                <Form.Item label="Name">
                  {getFieldDecorator("name", {
                    rules: [{ required: true, message: "Project name is required" }]
                  })(<Input className="activity-full-width" placeholder="Project name" />)}
                </Form.Item>
                <Form.Item label="Description">
                  {getFieldDecorator("description", {
                    rules: [{ required: false }]
                  })(<Input className="activity-full-width" placeholder="Project description" />)}
                </Form.Item>
                <Form.Item label="Type">
                  {getFieldDecorator("type", {
                    rules: [{ required: true, message: "Project type is required" }]
                  })(
                    <Select placeholder="Choose one of project type" className="activity-full-width">
                      {this.state.projectType
                        ? this.state.projectType.map((item, key) => {
                            return (
                              <Option key={key} value={item.name}>
                                {item.name}
                              </Option>
                            );
                          })
                        : null}
                    </Select>
                  )}
                </Form.Item>
                <Form.Item label="Target Date">
                  {getFieldDecorator("target_date", {
                    rules: [{ required: true, message: "Project due date is required" }]
                  })(<DatePicker placeholder="Project target date" className="activity-full-width" format="DD MMMM YYYY" />)}
                </Form.Item>
                <Form.Item label="Members">
                  {getFieldDecorator("members", {
                    rules: [{ required: false }]
                  })(
                    <Select placeholder="Add one or more users" className="activity-full-width" mode="multiple" loading={this.state.loadingMember}>
                      {this.state.dataMembers
                        ? this.state.dataMembers.map(item => {
                            return (
                              <Option key={item.id} value={item.id}>
                                {item.name}
                              </Option>
                            );
                          })
                        : null}
                    </Select>
                  )}
                </Form.Item>
              </Row>
              <Row>
                <Col span={5} style={{ float: "right" }}>
                  <Button type="primary" htmlType={"submit"} icon="save" loading={this.state.loadingCreate}>
                    Submit
                  </Button>
                </Col>
              </Row>
            </Form>
          ) : null}
        </Modal>
      </Row>
    );
  }
}
export default Form.create({ name: "create" })(Activity);
