import React from "react";
import { connect } from "react-redux";
import * as actions from "./../../store/user/actions";

// import Controller from "./controller";
import { Row, Col, Card, Button, Input, Typography, Divider, Form, Alert } from "antd";
import "./style.scss";
import { Redirect } from "react-router-dom";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields(async (err, values) => {
      this.props.login(values.username, values.password);
    });
  };

  render() {
    console.log(this.props.loading);
    console.log(this.props.user);
    const { getFieldDecorator } = this.props.form;
    if (localStorage.getItem("accessToken") !== null) {
      return <Redirect to="/dashboard" />;
    }
    return (
      <div>
        <Row type="flex" justify="space-around" align="middle" className="login-container">
          <Col span={7}>
            {this.state.error ? <Alert type="error" message={this.state.error} banner closable /> : null}
            <Form onSubmit={e => this.handleSubmit(e)}>
              <Card
                type="flex"
                title="Login"
                justify="space-around"
                align="middle"
                headStyle={{
                  color: "#136DF7",
                  fontFamily: "Helvetica",
                  fontSize: "20px",
                  lineHeight: "23px"
                }}
              >
                <Row type="flex" justify="space-around" align="middle" className="login-mt-4">
                  <Col span={18}>or use your email account</Col>
                </Row>
                <Row type="flex" justify="space-around" align="middle" className="login-mt-4">
                  <Col span={18}>
                    <Form.Item>
                      {getFieldDecorator("username", {
                        rules: [{ required: true, message: "masukkan username" }]
                      })(<Input size="large" placeholder="username" type="text" />)}
                    </Form.Item>
                  </Col>
                </Row>
                <Row type="flex" justify="space-around" align="middle">
                  <Col span={18}>
                    <Form.Item>
                      {getFieldDecorator("password", {
                        rules: [{ required: true, message: "masukkan password" }]
                      })(<Input.Password size="large" placeholder="password" />)}
                    </Form.Item>
                  </Col>
                </Row>
                <Row type="flex" justify="space-around" align="middle" className="login-mt-4">
                  <Col span={10}>
                    <Typography.Text strong>Forgot your password?</Typography.Text>
                    <Divider className="login-mt-2" style={{ border: "0.2px solid #000000" }} />
                  </Col>
                </Row>
                <Row type="flex" justify="space-around" align="middle" className="login-mt-3 login-margin-bottom">
                  <Col span={10}>
                    <Button type="primary" htmlType={"submit"} icon={this.state.isLoading ? "loading" : null} style={{ width: "70%" }} disabled={this.state.isLoading}>
                      {this.state.isLoading ? "" : "Login"}
                    </Button>
                  </Col>
                </Row>
              </Card>
            </Form>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { userReducer } = state;
  return {
    loading: userReducer.loading,
    user: userReducer.user
  };
};

const mapDispatchToProps = dispatch => ({
  login: (username, password) => dispatch(actions.login(username, password))
});

export default connect(mapStateToProps, mapDispatchToProps)(Form.create({ name: "create" })(Login));
