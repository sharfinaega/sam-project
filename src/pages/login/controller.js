// import React from "react";
// import Helper from "../../services/login";
// import { setHtmlStorage } from "../../services/helper";
// import history from "../../history";

// class LoginController extends React.Component {
//   constructor() {
//     super();
//     this.state = {
//       email: "",
//       password: "",
//       posts: [],
//       isLoading: false,
//       error: null
//     };
//   }
//   handleSubmit = async e => {
//     if (!this.state.isLoading) {
//       this.setState({
//         isLoading: true
//       });
//     }
//     e.preventDefault();
//     this.props.form.validateFields(async (err, values) => {
//       if (!err) {
//         const payload = values;
//         try {
//           const resp = await Helper.loginPosts(payload);
//           if (resp.success) {
//             this.setState({
//               isLoading: false
//             });
//             await this.setState({ posts: resp.data });
//             await setHtmlStorage("accessToken", this.state.posts.token);
//             await setHtmlStorage("user", JSON.stringify(this.state.posts.user));
//             history.push("/dashboard");
//           }
//         } catch (error) {
//           this.setState({
//             isLoading: false,
//             error: "Username/Password Salah!"
//           });
//         }
//       } else {
//         this.setState({
//           isLoading: false
//         });
//       }
//     });
//   };
// }

// export default LoginController;
