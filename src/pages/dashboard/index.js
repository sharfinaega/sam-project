import React from "react";
import Board from "@lourenci/react-kanban";
import { Row, Col, Button, Card, Select, DatePicker, Modal, Form, Input, Avatar } from "antd";
import "./style.scss";
import dashboardController from "./controller";
import RenderLaneAdder from "../../components/RenderLaneAdder";
import RenderLaneHeader from "../../components/RenderLaneHeader";
import RenderCardCanban from "../../components/RenderCardKanban";
import ModalTask from "../../components/ModalTask";

const { Option } = Select;
class Dashboard extends dashboardController {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    // const { getFieldDecorator } = this.props.form;
    const board = {
      lanes: [
        {
          id: 1,
          title: "PT Sentosa Indah",
          cards: [
            {
              id: 1,
              title: "Teaser PT Sentosa Indah",
              description: "Pengajuan Kredit Term 3"
            }
          ]
        },
        {
          id: 2,
          title: "SAM 1 Monthly Outing Event",
          cards: [
            {
              id: 2,
              title: "Konfirmasi: Alternatif tempat dan waktu",
              description: "12 March 2020"
            }
          ]
        },
        {
          id: 3,
          title: "Finish",
          cards: [
            {
              id: 3,
              title: "Pt Nusa Indah",
              description: "Pengajuan Kredit Term 1"
            }
          ]
        }
      ]
    };
    return (
      <div className="activity-layout-content">
        <Row type="flex" justify="space-between" align="middle" className="activity-card-filter">
          <Col xs={6} className="activity-title">
            Project Management
          </Col>
          <Col>
            <Row type="flex">
              <Col className="activity-btn-icon">
                Refresh
                <Button className="activity-btn-btn1" type="secondary" icon="reload" />
              </Col>
              Filter Board
              <Col className="activity-btn-icon2">
                <Button className="activity-btn-btn2" type="secondary" icon="filter" />
              </Col>
              <Modal title="Create Board" visible={this.state.visibleModalCreate} centered onCancel={this.showModalCreate} footer={false} className="activity-modal-container">
                {this.state.visibleModalCreate ? (
                  <Form onSubmit={e => this.handleSubmit(e)}>
                    <Row>
                      <Form.Item label="Name">{<Input className="activity-full-width" placeholder="Project name" />}</Form.Item>
                      <Form.Item label="Description">{<Input className="activity-full-width" placeholder="Project description" />}</Form.Item>
                      <Form.Item label="Type">
                        <Select placeholder="Choose one of project type" className="activity-full-width">
                          {this.state.projectType
                            ? this.state.projectType.map((item, key) => {
                                return (
                                  <Option key={key} value={item.name}>
                                    {item.name}
                                  </Option>
                                );
                              })
                            : null}
                        </Select>
                      </Form.Item>
                      <Form.Item label="Target Date">
                        <DatePicker placeholder="Project target date" className="activity-full-width" format="DD MMMM YYYY" />
                      </Form.Item>
                      <Form.Item label="Members">
                        <Select placeholder="Add one or more users" className="activity-full-width" mode="multiple" loading={this.state.loadingMember}>
                          {this.state.dataMembers
                            ? this.state.dataMembers.map(item => {
                                return (
                                  <Option key={item.id} value={item.id}>
                                    {item.name}
                                  </Option>
                                );
                              })
                            : null}
                        </Select>
                      </Form.Item>
                    </Row>
                    <Row>
                      <Col span={5} style={{ float: "right" }}>
                        <Button type="primary" htmlType={"submit"} icon="save" loading={this.state.loadingCreate}>
                          Submit
                        </Button>
                      </Col>
                    </Row>
                  </Form>
                ) : null}
              </Modal>
              <Col className="activity-btn-wrap">
                <Button type="primary" onClick={this.showModalCreate} style={{ float: "right" }}>
                  Create New Board
                </Button>
              </Col>
            </Row>
          </Col>
        </Row>

        <Col>
          <Board
            initialBoard={board}
            allowAddLane
            disableLaneDrag
            onLaneNew={console.log}
            onCardNew={() => console.log("ada yg baru nih")}
            onCardDragEnd={(board, source, destination) => this.onCardDragEnd(board, source, destination)}
            renderLaneAdder={({ addLane }) => <RenderLaneAdder addLane={addLane} />}
            renderCard={data => <RenderCardCanban data={data} onOpen={data => this.onOpen(data)} />}
            renderLaneHeader={({ id, title }, { addCard }) => <RenderLaneHeader title={title} onAddNewTask={name => this.addNewTask(id, name, addCard)} />}
          >
            {this.state.board}
          </Board>
          <ModalTask
            isOpen={this.state.isOpen}
            onCancel={() => this.onOpen()}
            // projectId={this.props.projectId}
            // data={this.state.dataTask}
            // receiveDataActivity={data => this.receiveDataActivity(data)}
            // dataMember={this.state.dataMember}
            // onSubmitChecklist={data => this.onSubmitChecklist(data)}
            // onSubmitChecklistItem={(data, index) => this.onSubmitChecklistItem(data, index)}
            // onCheckedChecklistItem={(indexChecklist, indexItem) => this.onCheckedChecklistItem(indexChecklist, indexItem)}
            // memberListActive={this.state.listCheckListMember}
            // changeMember={(data, index) => this.handleClickMember(data, index)}
            // updateDataTitle={title => this.updateDataTitle(title)}
            // updateDataDescription={description => this.updateDataDescription(description)}
            // updateDataDeadline={deadline => this.updateDataDeadline(deadline)}
            // createDataActivity={payload => this.createDataActivity(payload)}
            // deleteCheck={id => this.deleteCheck(id)}
            // deleteCheckItem={(id_item, id_cheklist) => this.deleteCheckItem(id_item, id_cheklist)}
          />
        </Col>
      </div>
    );
  }
}
export default Dashboard;
