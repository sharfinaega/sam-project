import React from "react";
import taskService from "../../services/project-task";

class dashboardController extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visibleModalCreate: false
    };
  }
  onOpen = data => {
    if (this.state.isOpen === false) {
      this.getTask(data);
      this.setState({ isOpen: true });
    } else {
      this.setState({ isOpen: false });
    }
  };
  getDataTask = async id => {
    const resp = await taskService.getTaskById(id);
    if (resp.success) {
      this.setState({ dataTask: resp.data });
    }
  };
  getTask = async data => {
    await this.getDataTask(data.id);
    await this.getUser();
    this.compareMember();
  };
  onCardDragEnd = async (board, source, destination) => {
    if (source.laneId !== destination.laneId) {
      const selectedBoard = board.lanes.find(b => {
        return b.id === destination.laneId;
      });
      const data = selectedBoard.cards[destination.index];
      const payload = {
        project_step_id: destination.laneId,
        title: data.title,
        description: data.description,
        deadline: data.deadline
      };
      await taskService.updateTask(data.id, payload);
    }
  };
  showModalCreate = () => {
    this.setState({
      visibleModalCreate: !this.state.visibleModalCreate
    });
  };
}

export default dashboardController;
